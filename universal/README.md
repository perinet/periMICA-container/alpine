# periMICA Alpine Container
The Alpine container facilitates the development of customized applications for periMICA. Install the container by clicking on `Continue`. After the installation, start the container by right-clicking on the container icon present on the home screen and selecting `Start`.

## changelog

- initial release of the container

## further documentation

For detailed information on how to use it and how to deploy your own customized web application, please check the [Perinet Documentation](https://docs.perinet.io/).
