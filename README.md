# alpine

## install container on periMICA

use the install dialog to upload and install the container

## running container on other systems

LXC and QEMU (on non armhf systems) are required
`sudo apt-get install lxc qemu-user-static`

OCI archives can be imported with the lxc oci template
`lxc-create -n alpine1 -t oci -- --url oci-archive:/tmp/periMICA-container-alpine-oci.tar`
