# instructions for local build of container

## use local buildah

buildah >= 1.26.0 is required

```
buildah bud -t ${CONTAINER_NAME}:${PACKAGE_VERSION} Containerfile
buildah push --format oci localhost/${CONTAINER_NAME}:${PACKAGE_VERSION} oci-archive:${PACKAGE_NAME}-oci.tar
```

## use buildah container

`podman run --privileged -v ${PWD}:/mnt quay.io/buildah/stable:v1.29.0 sh -c "buildah bud --platform linux/arm/v7 -t ${CONTAINER_NAME}:${PACKAGE_VERSION} /mnt/Containerfile && buildah push --format oci localhost/${CONTAINER_NAME}:${PACKAGE_VERSION} oci-archive:/mnt/${PACKAGE_NAME}-oci.tar"`

or

`docker run --privileged -v ${PWD}:/mnt quay.io/buildah/stable:v1.29.0 sh -c "buildah bud --platform linux/arm/v7 -t ${CONTAINER_NAME}:${PACKAGE_VERSION} /mnt/Containerfile && buildah push --format oci localhost/${CONTAINER_NAME}:${PACKAGE_VERSION} oci-archive:/mnt/${PACKAGE_NAME}-oci.tar && chmod 666 /mnt/${PACKAGE_NAME}-oci.tar"`

# create universal tar container from oci container

lxc is required

```
export ROOTFS_PATH="/var/lib/lxc/${CONTAINER_NAME}/rootfs"
lxc-create -n ${CONTAINER_NAME} -t oci -- --url oci-archive:${PACKAGE_NAME}-oci.tar
cp -r universal/* .
tar --numeric-owner -czf container.tar.gz -C ${ROOTFS_PATH} .
tar -cf ${PACKAGE_NAME}.tar container.tar.gz metadata.json script.json update_script.json README.md
```
