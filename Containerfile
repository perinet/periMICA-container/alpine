# redocly
FROM --platform=$BUILDPLATFORM docker.io/redocly/cli:1.25.4 AS ui_builder

### build API
RUN git clone --depth 1 --branch main https://gitlab.com/perinet/unified-api.git /usr/local/unified-api
WORKDIR /usr/local/unified-api

RUN redocly join --without-x-tag-groups --config /usr/local/unified-api/.redocly/redocly_config.yaml \
    -o /usr/local/unified-api/api.yaml \
    /usr/local/unified-api/src/_openapi.yaml \
    /usr/local/unified-api/src/base-services/Node_openapi.yaml \
    /usr/local/unified-api/src/base-services/Security_openapi.yaml \
    /usr/local/unified-api/src/base-services/LifeCycle_openapi.yaml \
    /usr/local/unified-api/src/base-services/DNS-SD_openapi.yaml

# RUN redocly build-docs openapi.yaml
RUN redocly build-docs --config /usr/local/unified-api/.redocly/index_config.yaml --template /usr/local/unified-api/.redocly/template.hbs -o /usr/local/unified-api/api.html /usr/local/unified-api/api.yaml


FROM --platform=$BUILDPLATFORM docker.io/library/golang:1.22.4-alpine3.19 AS builder

ARG TARGETOS
ARG TARGETARCH

RUN apk add git upx

RUN git clone --depth 1 --branch v1.0.1 https://gitlab.com/perinet/periMICA-container/service/base /usr/local/go/src/base
WORKDIR /usr/local/go/src/base
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -gcflags=all="-l -B" -ldflags="-w -s" -o output/perimica_webserver
RUN upx --best --lzma output/perimica_webserver


FROM --platform=$TARGETPLATFORM registry.gitlab.com/perinet/perimica-container/alpine-base:4

ARG TARGETPLATFORM

COPY webdesign/ /var/www/staticfiles/
COPY rootfs/ /
COPY README.md /META/

COPY --from=builder /usr/local/go/src/base/output/perimica_webserver /usr/bin/
COPY --from=ui_builder /usr/local/unified-api/api.yaml /var/www/staticfiles/api.yaml
COPY --from=ui_builder /usr/local/unified-api/api.html /var/www/staticfiles/api.html

RUN apk update
RUN apk --no-cache add openssh-server openssh-server-pam oath-toolkit-pam_oath oath-toolkit-oathtool dropbear-scp

RUN rc-update add oath-init default
RUN rc-update add sshd default
RUN rc-update add perimica_webserver default

RUN mkdir -p /etc/webserver
RUN chmod 755 /etc/webserver

RUN echo -e "PermitRootLogin yes\nUsePAM yes" >> /etc/ssh/sshd_config
RUN echo "auth sufficient pam_oath.so usersfile=/etc/users.oath window=30 digits=6" > /etc/pam.d/sshd.pam
RUN echo "auth sufficient pam_oath.so usersfile=/etc/users.oath window=30 digits=6" > /etc/pam.d/sshd

RUN apk -q list --installed
